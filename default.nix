let
  pkgs = import <nixpkgs> { };
  datafile = pkgs.fetchurl {
    url = "https://unicode.org/Public/emoji/13.0/emoji-test.txt";
    sha256 = "1fp05b3a1j0znvysjl38fcf7ksmj841flcf5y9kqn79fjrkabn6v";
  };
in
  pkgs.haskellPackages.callPackage ./emocli.nix { inherit datafile; }
