module TH (emojiListQ) where 

import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import System.Environment

-- local import
import FileParser
import Types

emojiListQ :: Q Exp
emojiListQ = do
  -- XXX: consider using addDependentFile to let ghci know about file changes
  datafilePath <- runIO $ getEnv "EMOCLI_DATAFILE"
  emojiStr <- runIO $ readFile datafilePath
  case parseEmoji emojiStr of
    [(ems, "")] -> lift ems
    _ -> undefined
    -- this match is intentionally incomplete
    -- in the hopes that it will fail on compile time
    -- if the emoji datafile does not parse cleanly
    -- XXX: does this even do what I think
