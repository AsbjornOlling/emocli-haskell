module FileParser (parseEmoji) where

import Text.ParserCombinators.ReadP
import Text.ParserCombinators.ReadP as ReadP (char)

import Types

{- DATAFILE PARSER
 - Parser for unicode emoji datafiles like the ones found at
 - https://unicode.org/Public/emoji/13.0/emoji-test.txt
 - (as of Mon 29 Jun 19:17:32 CEST 2020)
 -}


parseEmoji :: String -> [([Emoji], String)]
parseEmoji = readP_to_S parseAllEmoji


parseAllEmoji :: ReadP [Emoji]
parseAllEmoji = do
  -- parser to consume the entire unicode datafile
  parseNonEmojiLines
  emojis <- sepBy parseEmojiLine parseNonEmojiLines
  parseNonEmojiLines
  eof
  return emojis


parseEmojiLine :: ReadP Emoji
parseEmojiLine = do
  -- read until the next emoji
  codes <- parseCodes
  skipSpaces
  status <- parseStatus
  skipSpaces
  emojiStr <- parseEmojiStr
  skipSpaces
  versionStr <- parseVersionStr
  skipSpaces
  name <- parseName
  return $ Emoji { emoji = emojiStr
                 , name = name
                 , codes = codes
                 , version = versionStr
                 }


parseNonEmojiLines :: ReadP ()
parseNonEmojiLines =
  skipMany $ choice [parseCommentLine, parseEmptyLine]


parseCommentLine :: ReadP String
parseCommentLine = do
  -- read a line starting with '#', discard result
  -- XXX: ugh I feel like I could discard some parens here
  satisfy (== '#')
  manyTill anyChar (satisfy (== '\n'))


parseEmptyLine :: ReadP String
parseEmptyLine = 
  string "\n"


parseCode :: ReadP String
parseCode = do
  -- parse a string of uppercase hex characters terminated by a space
  code <- many1 (satisfy isUpperHex)
  satisfy (== ' ')
  return code


isUpperHex :: Char -> Bool
isUpperHex char =
  char `elem` "0123456789ABCDEF"


parseCodes :: ReadP [String]
parseCodes =
  -- parse multiple unicode hex codes
  many1 parseCode


parseStatus :: ReadP String
parseStatus = do
  string "; "
  choice [ string "component"
         , string "fully-qualified"
         , string "minimally-qualified"
         , string "unqualified"
         ]


parseEmojiStr :: ReadP String
parseEmojiStr =
  between (string "# ") (ReadP.char ' ') $ many1 (satisfy (/= ' '))


parseVersionStr :: ReadP String
parseVersionStr =
  manyTill (satisfy isVersionChar) (ReadP.char ' ')


isVersionChar :: Char -> Bool
isVersionChar char =
  char `elem` "0123456789.E"


parseName :: ReadP String
parseName =
  -- read the plain text name of the emoji
  -- and then consume all the whitespace at end of line
  manyTill anyChar parseEndOfLine


parseEndOfLine :: ReadP String
parseEndOfLine =
  manyTill (ReadP.char ' ') (ReadP.char '\n')


anyChar :: ReadP Char
anyChar = satisfy (const True)
