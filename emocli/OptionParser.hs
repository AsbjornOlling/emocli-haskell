module OptionParser (parseOptions) where

import Options.Applicative
import Data.Semigroup ((<>))

-- local imports
import Types


parseOptions :: IO Config
parseOptions =
  execParser $
    info (configParser <**> helper)
         ( fullDesc
        <> progDesc "Search for an emoji by name"
        <> header "emocli - a command-line emoji picker" )


configParser :: Parser Config
configParser = Config 
  <$> option auto
      ( long "results"
     <> short 'n'
     <> showDefault 
     <> value 5
     <> metavar "INT"
     <> help "Number of results to return" )
  <*> switch
      ( long "quiet"
     <> short 'q'
     <> help "Only output emoji" )
  <*> switch
      ( long "exact"
     <> short 'x'
     <> help "Same as \"--quiet --results 1\"" )
  <*> argument str ( metavar "EMOJI_SEARCH" )
