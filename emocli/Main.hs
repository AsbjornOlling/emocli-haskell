{-# LANGUAGE TemplateHaskell #-}

{- emocli (working title)
 - is a handy-dandy emoji tool written in Haskell.
 - It is also my first haskell project.
 -}

module Main where

-- standard stuff
import Data.List
import System.Environment

-- external dependencies
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Text.Fuzzy as Fuzzy (filter, Fuzzy, original)

-- local imports
import OptionParser
import FileParser
import Types
import TH


main :: IO ()
main = do
  -- read cli args
  conf <- parseOptions

  -- do search
  let n = if exact conf then 1 else results conf
  let searchResults = take n $ fuzzySearch $ searchTerm conf

  -- make and print str
  let toStr = if quiet conf || exact conf then emoji else strWithName
  let str = intercalate "\n" $ map toStr searchResults
  putStr str


strWithName :: Emoji -> String
strWithName em =
  emoji em ++ "\t" ++ name em


fuzzySearch :: String -> [Emoji]
fuzzySearch term =
   map original $ Fuzzy.filter term emojiList "" "" name False


emojiList :: [Emoji]
emojiList = $(emojiListQ)
