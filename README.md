
# emocli

**emocli** searches emoji by name, and prints the results to stdout.

That's it.

## Example usage

```
$ emocli "woman technologist"
👩‍💻	woman technologist
👩🏻‍💻	woman technologist: light skin tone
👩🏼‍💻	woman technologist: medium-light skin tone
👩🏽‍💻	woman technologist: medium skin tone
👩🏾‍💻	woman technologist: medium-dark skin tone

$ emocli -n 10 cat
😺	grinning cat
😸	grinning cat with smiling eyes
😹	cat with tears of joy
😻	smiling cat with heart-eyes
😼	cat with wry smile
😽	kissing cat
🙀	weary cat
😾	pouting cat
🐱	cat face
🐈	cat

$ emocli -x "family woman woman boy"
👩‍👩‍👦
```
